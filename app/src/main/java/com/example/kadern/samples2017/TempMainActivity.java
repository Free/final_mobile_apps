package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnIntentSender;
    Button btnUserList;
    Button btnAdapters;
    Button btnLifeCycle;
    Button btnWebService;
    Button btnSQLite;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();
            switch(buttonResourceId){
                case R.id.btnImgSpinner:
                    startActivity(new Intent(TempMainActivity.this, ImageSpinnerActivity.class));
                    break;
                case R.id.btnIntentSender:
                    startActivity(new Intent(TempMainActivity.this, IntentSenderActivity.class));
                    break;
                case R.id.btnUserList:
                    startActivity(new Intent(TempMainActivity.this, UserListActivity.class));
                    break;
                case R.id.btnAdapters:
                    startActivity(new Intent(TempMainActivity.this, Adapters.class));
                    break;
                case R.id.btnLifeCycle:
                    startActivity(new Intent(TempMainActivity.this, LifeCycleActivity.class));
                    break;
                case R.id.btnWebService:
                    startActivity(new Intent(TempMainActivity.this, WebServiceActivity.class));
                    break;
                case R.id.btnSQLite:
                    startActivity(new Intent(TempMainActivity.this, SQLiteActivity.class));
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button)findViewById(R.id.btnImgSpinner);
        btnImgSpinner.setOnClickListener(listener);

        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnIntentSender.setOnClickListener(listener);

        btnUserList = (Button)findViewById(R.id.btnUserList);
        btnUserList.setOnClickListener(listener);

        btnAdapters = (Button)findViewById(R.id.btnAdapters);
        btnAdapters.setOnClickListener(listener);

        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnLifeCycle.setOnClickListener(listener);

        btnWebService = (Button)findViewById(R.id.btnWebService);
        btnWebService.setOnClickListener(listener);

        btnSQLite = (Button)findViewById(R.id.btnSQLite);
        btnSQLite.setOnClickListener(listener);

    }


}

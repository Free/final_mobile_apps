package com.example.kadern.samples2017;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

public class UserDetailsActivity extends AppCompatActivity {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String USER_ID_EXTRA = "userid";

    AppClass app;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;

    Button btnClearForm;
    Button btnInsertData;
    Button btnDeleteUser;

    UserDetailsHelper dbHelper;
    UserDataAccess da;

    public String DBSTATE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        app =(AppClass)getApplication();
        //grab vars
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA,-1);
        if(userId >= 0){
            //Toast.makeText(this, "GET USER: " + userId, Toast.LENGTH_LONG).show();
        }

        //DB state
        DBSTATE = i.getStringExtra("DBSTATE");
        if(DBSTATE == null) {
            DBSTATE = "ADD";
        }

        //db stuff
        dbHelper = new UserDetailsHelper(this);
        da = new UserDataAccess(dbHelper);

        //Get user
        user = app.getUserByID(userId);
        if(DBSTATE.equals("EDIT")) {
            putDataInUI();
        }


        //SAVE BUTTON
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromUI();

                //EDITTING EXITING USER
                if(DBSTATE.equals("EDIT")) {
                    if(validateForms()) {
                        da.updateUser(user);
                        startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                    }

                }
                //NEW USER
                else {
                    if(validateForms()) {
                        da.insertItem(user);
                        startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                    }

                }


            }
        });

        //CLEAR FORM BUTTON
        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        //INSERT DATA BUTTON
        btnInsertData = (Button)findViewById(R.id.btnInsertData);
        btnInsertData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //New user, no point in putting data in
                if(DBSTATE.equals("EDIT")) {
                    putDataInUI();
                }
            }
        });

        //DELETE USER BUTTON
        btnDeleteUser = (Button)findViewById(R.id.btnDeleteUser);
        btnDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DBSTATE.equals("EDIT")) {
                    da.deleteUser(user);

                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                }
            }
        });
    }

    //Function to validate input
    public boolean validateForms() {

        //Grab vars
        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        //FIRST NAME VALIDATION
        if(firstName.length() < 1) {
            toast("FIRST NAME MISSING");
            return false;
        }

        //EMAIL VALIDATION
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if(email.length() < 1 || !matcher.matches()) {
            toast("EMAIL INVALID");
            return false;
        }

        //MUSIC VALIDATION
        if(selectedRadioButtonId == -1) {
            toast("FAVORITE MUSIC MISSING");
            return false;
        }

        return true;
    }


    //Shorthand function to toast things
    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void getDataFromUI(){

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        if(user != null) {
            user.setFirstName(firstName);
            user.setEmail(email);
            user.setFavoriteMusic(favoriteMusic);
            user.setActive(active);

        } else {
            user = new User(1, firstName, email, favoriteMusic, active);
        }
        //Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();
    }

    private void putDataInUI(){

        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()){
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }
}

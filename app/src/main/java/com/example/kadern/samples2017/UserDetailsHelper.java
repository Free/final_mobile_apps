package com.example.kadern.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

/**
 * Created by 003012468 on 11/15/2017.
 */

public class UserDetailsHelper extends SQLiteOpenHelper {

    private static final String TAG = "UserDetailsHelper";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    public UserDetailsHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = UserDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}

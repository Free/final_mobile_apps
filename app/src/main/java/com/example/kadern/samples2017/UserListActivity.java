package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

public class UserListActivity extends AppCompatActivity {

    AppClass app;
    ListView userListView;
    Button btnAddNewUser;

    UserDetailsHelper dbHelper;
    UserDataAccess da;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        //ADD USER BUTTON
        btnAddNewUser = (Button)findViewById(R.id.btnAddNewUser);
        btnAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserListActivity.this, UserDetailsActivity.class));
            }
        });


        app = (AppClass)getApplication();

        userListView = (ListView)findViewById(R.id.userListView);

        //setup db
        dbHelper = new UserDetailsHelper(this);
        da = new UserDataAccess(dbHelper);

        //grab info from db - returns users and populates app.users
        app.users = da.getAllUsers();



        ArrayAdapter adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, app.users){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View listItemView = super.getView(position, convertView, parent);
                User currentUser = app.users.get(position);

                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                CheckBox chk = (CheckBox)listItemView.findViewById(R.id.chkActive);
                Button editBtn = (Button)listItemView.findViewById(R.id.editUserButton);

                //Set tag to edit button
                editBtn.setTag(currentUser);


                //Grab vars
                lbl.setText(currentUser.getFirstName());
                chk.setChecked(currentUser.isActive());
                chk.setText("Active");
                chk.setTag(currentUser);

                //Active user event
                chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        CheckBox c = (CheckBox)view;
                        selectedUser.setActive(c.isChecked());
                    }
                });

                //Edit button event
                editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                        i.putExtra("DBSTATE", "EDIT");
                        startActivity(i);
                    }
                });

                return listItemView;

            }

        };

        userListView.setAdapter(adapter);

    }
}

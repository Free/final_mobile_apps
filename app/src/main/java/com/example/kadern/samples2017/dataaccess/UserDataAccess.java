package com.example.kadern.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.kadern.samples2017.UserDetailsHelper;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by 003012468 on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";
    private UserDetailsHelper dbHelper;
    private SQLiteDatabase database;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_FIRST_NAME = "user_first_name";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_FAVORITE_MUSIC = "user_music";
    public static final String COLUMN_USER_ACTIVE = "user_active";

    //creating the table
    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_FIRST_NAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_USER_FAVORITE_MUSIC,
                    COLUMN_USER_ACTIVE
            );

    //User data access
    public UserDataAccess(UserDetailsHelper dbHelper){
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }

    public User insertItem(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_FIRST_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());

        Log.d("TEST", u.toString());

        long insertId = database.insert(TABLE_NAME, null, values);

        u.setId(insertId);
        return u;
    }

    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID,
                COLUMN_USER_FIRST_NAME,
                COLUMN_USER_EMAIL,
                COLUMN_USER_FAVORITE_MUSIC,
                COLUMN_USER_ACTIVE,
                TABLE_NAME
        );

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            User user = new User();
            //ID
            user.setId(c.getLong(0));
            //FN
            user.setFirstName(c.getString(1));
            //EMAIL
            user.setEmail(c.getString(2));
            //changing the database data-type String to Music
            User.Music um = null;
            //MUSIC
            String str = c.getString(3);
            if(str.equals("JAZZ")){
                um = User.Music.JAZZ;
            } else if(str.equals("RAP")){
                um = User.Music.RAP;
            } else if (str.equals("COUNTRY")){
                um = User.Music.COUNTRY;
            } else {

            }
            user.setFavoriteMusic(um);

            //ACTIVE
            String active = c.getString(4);
            if(active.equalsIgnoreCase("1")) {
                user.setActive(true);
            } else {
                user.setActive(false);
            }

//
//            user.isActive();


            users.add(user);
            c.moveToNext();
        }
        c.close();
        return users;
    }

    //update Users
    public User updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_FIRST_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
//        values.put(COLUMN_USER_ACTIVE, u.getId());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + u.getId(), null);

        return u;
    }

    //deleting User
    public int deleteUser(User u){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);

        return rowsDeleted;
    }
}
